# CFT

This repo is the home of CFT specific code and services. Please see go/cft-docs for detailed information about CFT, how to author services, have them build, etc.
