// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Local constants to FoilProvision CLI
package constants

const (
	DefaultPort         = 80
	DefaultLogDirectory = "/tmp/provisionservice/"
)
