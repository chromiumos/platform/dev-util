// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
package main

import (
	"log"
	"os"
)

func main() {
	log.Println("test_finder_filter has been moved to https://chromium.googlesource.com/infra/infra/+/refs/heads/main/go/src/infra/cros/cmd/ctpv2-filters/test_finder_filter/")

	os.Exit(0)
}
