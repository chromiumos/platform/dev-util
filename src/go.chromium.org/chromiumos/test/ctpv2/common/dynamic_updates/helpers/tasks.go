// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package helpers

import (
	_go "go.chromium.org/chromiumos/config/go"
	"go.chromium.org/chromiumos/config/go/test/api"
	"go.chromium.org/chromiumos/test/ctpv2/common/dynamic_updates/common"
	"go.chromium.org/chromiumos/test/ctpv2/common/dynamic_updates/interfaces"
	"google.golang.org/protobuf/types/known/anypb"
)

// DefaultProvisionStartUpRequest outlines the default values
// typically required for starting up a provision task.
func DefaultProvisionStartUpRequest(deviceId *common.DeviceIdentifier) *interfaces.ProvisionTaskStartUpRequest {
	servoTaskIdentifier := common.NewTaskIdentifier(common.ServoNexus).AddDeviceId(deviceId)
	return &interfaces.ProvisionTaskStartUpRequest{
		DynamicInputs: map[string]string{
			"dut":            deviceId.GetDevice("dut"),
			"dutServer":      deviceId.GetCrosDutServer(),
			"servoNexusAddr": servoTaskIdentifier.Id,
		},
	}
}

// DefaultCrosProvisionInstallRequest sets up a cros-provision request
// with an empty metadata field, solely passing along the installPath.
func DefaultCrosProvisionInstallRequest(installPath string) *interfaces.ProvisionTaskInstallRequest {
	crosProvisionMetadata, _ := anypb.New(&api.CrOSProvisionMetadata{})
	return &interfaces.ProvisionTaskInstallRequest{
		StaticImagePath: &_go.StoragePath{
			HostType: _go.StoragePath_GS,
			Path:     installPath,
		},
		StaticMetadata: crosProvisionMetadata,
	}
}

// DefaultAndroidProvisionInstallRequest sets up a android-provision request.
func DefaultAndroidProvisionInstallRequest() *interfaces.ProvisionTaskInstallRequest {
	androidProvisionMetadata, _ := anypb.New(&api.AndroidProvisionRequestMetadata{
		CipdPackages: []*api.CIPDPackage{
			{
				VersionOneof: &api.CIPDPackage_Ref{
					Ref: "latest_stable",
				},
				AndroidPackage: api.AndroidPackage_GMS_CORE,
			},
		},
	})
	return &interfaces.ProvisionTaskInstallRequest{
		StaticMetadata: androidProvisionMetadata,
	}
}
