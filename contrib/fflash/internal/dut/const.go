// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package dut

const (
	statefulDir        = "/mnt/stateful_partition"
	statefulAvailable  = ".update_available"
	powerdLockFilePath = "/run/lock/power_override/fflash.lock"
)
