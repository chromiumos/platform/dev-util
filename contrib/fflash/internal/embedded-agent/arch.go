// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package embeddedagent

const (
	ArchAarch64 = "aarch64"
	ArchX8664   = "x86_64"
)
