# How to generate a standalone script for installing SoftAP to Ubuntu host

For the internal process to generate a new standalone script,
see
[go/cros-wifi-softap-ubuntu](http://go/cros-wifi-softap-ubuntu)

* [cros-install-wifi-to-host-template.sh](./cros-install-wifi-to-host-template.sh)
The template will be used to generate a standalone script run on a dev machine.
* [cros-wifi-install.sh](./cros-wifi-install.sh) This is the script to be uploaded
to the target host.
