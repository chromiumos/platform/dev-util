#!/bin/bash
# Copyright 2019 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

VERSION="1.5.0"
SCRIPT=$(basename -- "${0}")
set -e

export LC_ALL=C

if [[ ! -e /etc/cros_chroot_version ]]; then
  echo "This script must be run inside the chroot."
  exit 1
fi

if [[ "$#" -lt 2 ]]; then
  echo "Usage: ${SCRIPT} reference_name variant_name [trunk_name] [bug_number]"
  echo "e.g. ${SCRIPT} hatch kohaku chromiumos b:140261109"
  echo "Creates the initial EC image as a copy of the reference board's EC."
  echo "trunk_path is an optional parameter, script uses 'chromiumos' as default path"
  exit 1
fi

# shellcheck source=check_standalone.sh
# shellcheck disable=SC1091
source "${BASH_SOURCE%/*}/check_standalone.sh"
check_standalone

# shellcheck source=check_pending_changes.sh
# shellcheck disable=SC1091
source "${BASH_SOURCE%/*}/check_pending_changes.sh"

# This is the name of the reference board that we copying to make the variant.
# ${var,,} converts to all lowercase.
REF="${1,,}"
# This is the name of the variant that is being cloned.
VARIANT="${2,,}"
# This is the trunk path for the source. Default path will be 'chromiumos' if not specified
TRUNK=${3:-"chromiumos"}
# Assign BUG= text, or "None" if that parameter wasn't specified.
BUG=${4:-None}

# Assign the value for BRANCH= in the commit message, or use None if unspecified
COMMIT_MSG_BRANCH="${NEW_VARIANT_BRANCH:-None}"

# All of the necessary files are in platform/ec/board
cd "${HOME}/${TRUNK}/src/platform/ec/board"

# Make sure that the reference board exists.
if [[ ! -e "${REF}" ]]; then
  echo "${REF} does not exist; please specify a valid reference board."
  exit 1
fi

# Make sure the variant doesn't already exist.
if [[ -e "${VARIANT}" ]]; then
  echo "${VARIANT} already exists; have you already created this variant?"
  exit 1
fi

# If there are pending changes, exit the script (unless overridden)
check_pending_changes "$(pwd)"

# Start a branch. Use YMD timestamp to avoid collisions.
DATE=$(date +%Y%m%d)
BRANCH="create_${VARIANT}_${DATE}"
# Store current branch information to restore in case of failure
CURRENT_BRANCH=`git describe --all | cut -d '/' -f 2-3`
# Checkout new branch to work on.
# In case script has already been running, skip new branch creation.
if [ -z "${NEW_VARIANT_WIP}" ]; then
    git checkout -b "${BRANCH}" -t "${CURRENT_BRANCH}"
fi

cleanup() {
  # If there is an error after the `repo start`, then remove the added files
  # and `repo abandon` the new branch.
  cd "${HOME}/${TRUNK}/src/platform/ec/board"
  if [[ -e "${VARIANT}" ]] ; then
    rm -Rf "${VARIANT}"
    # Use || true so that if the new files haven't been added yet, the error
    # won't terminate the script before we can finish cleaning up.
    git restore --staged "${VARIANT}" || true
  fi
  git checkout "${CURRENT_BRANCH}"
  git branch -D "${BRANCH}"
}
trap 'cleanup' ERR

mkdir "${VARIANT}"
cp "${REF}"/* "${VARIANT}"

# Update copyright notice to current year.
YEAR=$(date +%Y)
find "${VARIANT}" -type f -exec \
    sed -i -e "s/Copyright.*20[0-9][0-9]/Copyright ${YEAR}/" {} +

# Build the code; exit if it fails.
pushd ..
make -j BOARD="${VARIANT}"
popd

git add "${VARIANT}"/*

# Now commit the files. Use fmt to word-wrap the main commit message.
MSG=$(echo "Create the initial EC image for the ${VARIANT} variant
by copying the ${REF} reference board EC files into a new
directory named for the variant." | fmt -w 70)

git commit -sm "${VARIANT}: Initial EC image

${MSG}

(Auto-Generated by ${SCRIPT} version ${VERSION}).

BUG=${BUG}
BRANCH=${COMMIT_MSG_BRANCH}
TEST=make BOARD=${VARIANT}"
