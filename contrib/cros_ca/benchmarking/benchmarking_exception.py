# Copyright 2024 The Chromium Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""Script contains Benchmarking Exception Class."""


class BenchmarkingException(Exception):
    """A custom exception that will be raised when there is exception in
    benchmarking operation.
    """
