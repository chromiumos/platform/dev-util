#!/usr/bin/env python3
# Copyright 2021 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""This script is deprecated."""

import sys


sys.exit(
    """This script is not used anymore. Follow these instructions instead:
https://chromium.googlesource.com/chromiumos/third_party/rust_crates/+/HEAD/README.md"""
)
